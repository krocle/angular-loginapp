import { FlashMessagesService } from 'angular2-flash-messages';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public email: string;
  public password: string;
  public res = null;
  public err  = null;


  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    private _flashMessagesService: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onSubmitLogin() {
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password)
    .then(res => {
      this._flashMessagesService.show('Usuario o Contraseña incorrecta', { cssClass: 'alert-success'});
      this.router.navigate(['/privado']);
    }).catch (err => {
      this.router.navigate(['/login']);
     this._flashMessagesService.show('Usuario o Contraseña incorrecta', { cssClass: 'alert-danger', timeout: 4000});
    });
  }

  onClickGoogleLogin() {
    this.afAuth.auth.signInWithPopup (new auth.GoogleAuthProvider)
    .then ((res) => {
      this._flashMessagesService.show('Usuario o Contraseña incorrecta', { cssClass: 'alert-success'});
      this.router.navigate(['/privado']);
      }).catch (err => {
        this.router.navigate(['/login']);
       this._flashMessagesService.show('Usuario o Contraseña incorrecta', { cssClass: 'alert-danger', timeout: 4000});
      });
  }
  onClickFacebookLogin() {
  }
  onClickTwitterLogin() {
  }

}
