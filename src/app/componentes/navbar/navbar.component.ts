import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { AuthService } from './../../servicios/auth.service';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;

  constructor(
    public router: Router,
    public afAuth: AngularFireAuth
  ) {
    this.user = afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;
          console.log(this.userDetails);
        } else {
          this.userDetails = null;
        }
      }
    );
  }

  ngOnInit() {
  }

  onClickLogout() {
    this.afAuth.auth.signOut();
    this.router.navigate(['/login']);
  }
  isLoggin() {
    if (this.userDetails == null ) {
      return false;
    } else {
      return true;
    }
  }

}
